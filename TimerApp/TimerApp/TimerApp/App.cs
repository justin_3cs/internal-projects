﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Xamarin.Forms;

namespace TimerApp
{

    class TimerState
    {        
        public Timer Tmr;
        public DateTime TimeStart = DateTime.MinValue;
        public TimeSpan AddTime = new TimeSpan(0);
    }

	public class App : Application
	{
        // Controls
	    Button _startButton;
	    Button _stopButton;
	    Label _timerLabel;
	    TimerState _timerState;	    

        public App ()
        {
            InitializeControls();

            // The root page of your application
            MainPage = new ContentPage {
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
                    Spacing = 10,
                    Padding = new Thickness(10,10,10,10),
					Children = {
						_timerLabel,                     
                        new StackLayout()
                        {                            
                            HorizontalOptions = LayoutOptions.Center,
                            Orientation = StackOrientation.Horizontal,
                            Children =
                            {
                               _startButton,
                               _stopButton                                
                            }
                        }
					}
				}
			};
		}

	    private void InitializeControls()
	    {
	        _timerLabel = new Label { XAlign = TextAlignment.Center, FontSize = 20 };
            _timerLabel.SetBinding(Label.TextProperty, "Text");
            _timerLabel.Text = "Welcome to Launch Timer!";

            _startButton = new Button { Text = "Start", HorizontalOptions = LayoutOptions.Center };
            _stopButton = new Button { Text = "Clear", HorizontalOptions = LayoutOptions.Center };

            _startButton.SetBinding(Button.TextProperty, "Text");
	        _startButton.Text = "Start";

	        _timerState = new TimerState();	                  
	      
	        _startButton.Clicked += (sender, args) =>
	        {                
                _timerLabel.Text = string.Format("{0:D2}:{1:D2}:{2:D2}:{3:D3}", 0, 0, 0, 0);

	            if (_startButton.Text == "Start")
	            {
	                _startButton.Text = "Stop";
                    _timerState.TimeStart = DateTime.Now;

	                if (_timerState.Tmr == null)	                
                        _timerState.Tmr = new Timer(CheckTimer, _timerState, 0, 50);	                     	                
	                else
                        _timerState.Tmr.Change(0, 50);
	            }
	            else
	            {
                    _timerState.Tmr.Change(0, 0);
                    _startButton.Text = "Start";
	                _timerState.AddTime = (DateTime.Now - _timerState.TimeStart) + _timerState.AddTime;                    
                    _timerLabel.Text = GetTimerString(DateTime.Now - _timerState.TimeStart + _timerState.AddTime);
                }	            
	        };

	        _stopButton.Clicked += (sender, args) =>
	        {
                _timerState.Tmr.Change(0, 0);
                _timerState.AddTime = new TimeSpan(0);
	            _timerState.TimeStart = DateTime.Now;                
                _timerLabel.Text = string.Format("{0:D2}:{1:D2}:{2:D2}:{3:D3}", 0, 0, 0, 0);                
	        };
	    }

	    private void CheckTimer(Object state)
	    {
	        var tState = (TimerState) state;
	        var span = (DateTime.Now - tState.TimeStart) + tState.AddTime;

            Device.BeginInvokeOnMainThread(() =>
            {
                _timerLabel.Text = GetTimerString(span);
            });            
            
	    }

	    private string GetTimerString(TimeSpan span)
	    {
	        return string.Format("{0:D2}:{1:D2}:{2:D2}:{3:D3}", span.Hours, span.Minutes, span.Seconds, span.Milliseconds);
        }


		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
